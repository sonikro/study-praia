

interface Veiculo {
    acelerar(): void;
}

export default function acelerarVeiculo(veiculo: Veiculo){
    veiculo.acelerar()
    window.alert('Veiculo acelerou')
}