'use strict';

import _ from 'lodash';
import './style.css'
import acelerarVeiculo from './modulo';
import fetch from 'isomorphic-fetch';
import { GenerateObservable } from 'rxjs/observable/GenerateObservable';
import { defer } from 'rxjs/observable/defer';

function component() {
    var element = document.createElement('div');

    // Lodash, currently included via a script, is required for this line to work
    element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    element.classList.add('hello');
    return element;
}
  
document.body.appendChild(component());

class Person {

    constructor(nome, idade){
        this._nome = nome;
        this._idade = idade;
    }

    get nome(){
        return this._nome;
    }

    set nome(nome){
        if (!nome) {
            throw Error('Nome não informado')
        }
        this._nome = nome;
    }

    seApresentar(){
        window.alert(`Meu nome é ${this.nome} e eu tenho ${this._idade} anos`)
    }

}

export class Teacher extends Person {

    seApresentar(){
        window.alert(`Meu nome é ${this.nome} e eu sou um professor`)
    }
}

const seApresentar = () => {
    window.alert(`Meu nome é ${this.nome} e eu sou um professor`)
}
const jonathan = new Person('Jonathan', 22)
const kaue = new Person('Kaue', 24);


acelerarVeiculo({acelerar: () => true})


for (let index = 0; index < array.length; index++) {
    const element = array[index];
    
}

for (const key in jonathan) {
    if (object.hasOwnProperty(key)) {
        const element = object[key];
        

    }

}

for (const iterator of object) {
    
}

const chamadaApi = async () => {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts/1')
    return await response.json()
}

const apiObservable = defer(chamadaApi)

apiObservable.subscribe((response) => window.alert(JSON.stringify(response)))